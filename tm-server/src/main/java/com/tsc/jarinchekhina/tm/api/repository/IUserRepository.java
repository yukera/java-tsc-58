package com.tsc.jarinchekhina.tm.api.repository;

import com.tsc.jarinchekhina.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.List;

public interface IUserRepository {

    void remove(@NotNull User user);

    void removeById(@NotNull String id);

    void removeByLogin(@NotNull String login);

    void add(@NotNull User user);

    void addAll(@NotNull Collection<User> collection);

    void update(@NotNull User user);

    void clear();

    @NotNull
    List<User> findAll();

    @Nullable
    User findById(@NotNull String id);

    @Nullable
    User findByLogin(@NotNull String login);

    @Nullable
    User findByEmail(@NotNull String email);

    @NotNull
    EntityManager getEntityManager();

}
