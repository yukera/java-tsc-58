package com.tsc.jarinchekhina.tm.listener.system;

import com.tsc.jarinchekhina.tm.event.ConsoleEvent;
import com.tsc.jarinchekhina.tm.listener.AbstractListener;
import com.tsc.jarinchekhina.tm.util.DataUtil;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import java.util.Collection;

@Component
public final class HelpListener extends AbstractListener {

    @NotNull
    @Override
    public String arg() {
        return "-h";
    }

    @NotNull
    @Override
    public String name() {
        return "system-help";
    }

    @NotNull
    @Override
    public String description() {
        return "display list of possible actions";
    }

    @Override
    @EventListener(condition = "@helpListener.name() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.out.println("[HELP]");
        @NotNull final Collection<AbstractListener> commands = serviceLocator.getCommandService().getCommands();
        for (@NotNull final AbstractListener command : commands) {
            @NotNull String result = "";
            if (!DataUtil.isEmpty(command.name())) result += command.name() + " ";
            if (!DataUtil.isEmpty(command.arg())) result += "(" + command.arg() + ") ";
            if (!DataUtil.isEmpty(command.description())) result += "- " + command.description();
            System.out.println(result);
        }
    }

}
