package com.tsc.jarinchekhina.tm.listener;

import com.tsc.jarinchekhina.tm.api.entity.IHasNameMethod;
import com.tsc.jarinchekhina.tm.api.service.IServiceLocator;
import com.tsc.jarinchekhina.tm.event.ConsoleEvent;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;

@Component
@NoArgsConstructor
public abstract class AbstractListener implements IHasNameMethod {

    @NotNull
    protected IServiceLocator serviceLocator;

    public void setServiceLocator(@NotNull final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public abstract void handler(ConsoleEvent event);

    @Nullable
    public abstract String arg();

    @Nullable
    public abstract String name();

    @Nullable
    public abstract String description();

}
