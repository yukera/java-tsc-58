package com.tsc.jarinchekhina.tm.listener.system;

import com.tsc.jarinchekhina.tm.event.ConsoleEvent;
import com.tsc.jarinchekhina.tm.listener.AbstractListener;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public final class ExitListener extends AbstractListener {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "system-exit";
    }

    @NotNull
    @Override
    public String description() {
        return "close application";
    }

    @Override
    @EventListener(condition = "@exitListener.name() == #event.name")
    public void handler(final ConsoleEvent event) {
        System.exit(0);
    }

}
